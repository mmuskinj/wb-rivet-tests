#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/InvisibleFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/TauFinder.hh"

namespace Rivet
{

    /// @brief W + jets production at 8 TeV
    class WJets_test : public Analysis
    {
    public:
        DEFAULT_RIVET_ANALYSIS_CTOR(WJets_test);

        // Book histograms and initialise projections before the run
        void init()
        {
            // Get taus
            TauFinder taus_had(TauFinder::DecayMode::HADRONIC);
            declare(taus_had, "TausHad");

            TauFinder taus_lep(TauFinder::DecayMode::LEPTONIC);
            declare(taus_lep, "TausLep");

            // Get neutrinos for MET calculation
            declare(InvisibleFinalState(true, true), "Neutrinos"); // true, true = only allow prompt invisibles and allow tau decays

            // HadTau histograms
            book(_h["Had__lep_pt"], "Had__lep_pt", 40, 0, 400);
            book(_h["Had__W_pt"], "Had__W_pt", 40, 0, 400);
            book(_h["Had__mT"], "Had__mT", 40, 0, 400);
            book(_h["Had__MET"], "Had__MET", 40, 0, 400);
            book(_h["Had__max_lep_pt_MET"], "Had__max_lep_pt_MET", 40, 0, 400);

            // HadTau histograms for MET > 75 GeV
            book(_h["Had__75MET__lep_pt"], "Had__75MET__lep_pt", 40, 0, 400);
            book(_h["Had__75MET__W_pt"], "Had__75MET__W_pt", 40, 0, 400);
            book(_h["Had__75MET__mT"], "Had__75MET__mT", 40, 0, 400);
            book(_h["Had__75MET__MET"], "Had__75MET__MET", 40, 0, 400);
            book(_h["Had__75MET__max_lep_pt_MET"], "Had__75MET__max_lep_pt_MET", 40, 0, 400);

            // LepTau histograms
            book(_h["Lep__lep_pt"], "Lep__lep_pt", 40, 0, 400);
            book(_h["Lep__W_pt"], "Lep__W_pt", 40, 0, 400);
            book(_h["Lep__mT"], "Lep__mT", 40, 0, 400);
            book(_h["Lep__MET"], "Lep__MET", 40, 0, 400);
            book(_h["Lep__max_lep_pt_MET"], "Lep__max_lep_pt_MET", 40, 0, 400);

            // LepTau histograms for MET > 75 GeV
            book(_h["Lep__75MET__lep_pt"], "Lep__75MET__lep_pt", 40, 0, 400);
            book(_h["Lep__75MET__W_pt"], "Lep__75MET__W_pt", 40, 0, 400);
            book(_h["Lep__75MET__mT"], "Lep__75MET__mT", 40, 0, 400);
            book(_h["Lep__75MET__MET"], "Lep__75MET__MET", 40, 0, 400);
            book(_h["Lep__75MET__max_lep_pt_MET"], "Lep__75MET__max_lep_pt_MET", 40, 0, 400);
        }

        // Perform the per-event analysis
        void analyze(const Event &event)
        {
            // retrieve the dressed electrons
            const Particles &taus_had = apply<TauFinder>(event, "TausHad").particlesByPt();
            const Particles &taus_lep = apply<TauFinder>(event, "TausLep").particlesByPt();
            if (taus_had.size() + taus_lep.size() != 1)
            {
                vetoEvent;
            }

            // store the tau
            Particle lepton;
            std::string channel = "";
            if (taus_had.size() == 1)
            {
                lepton = taus_had.at(0);
                channel = "Had";
            }
            else
            {
                lepton = taus_lep.at(0);
                channel = "Lep";
            }

            // calulate MET and mT
            const Particles &invisibles = apply<InvisibleFinalState>(event, "Neutrinos").particles();
            FourMomentum pMET = sum(invisibles, Kin::mom, FourMomentum()).setZ(0);
            const double MET = pMET.pT() / GeV;
            const double mT = sqrt(2 * lepton.Et() / GeV * MET * (1 - cos(deltaPhi(lepton, pMET))));

            // calculate the observables
            const double w_pt = (lepton.momentum() + pMET).pT() / GeV;

            // fill histograms
            _h[channel + "__lep_pt"]->fill(lepton.Et() / GeV);
            _h[channel + "__W_pt"]->fill(w_pt);
            _h[channel + "__mT"]->fill(mT);
            _h[channel + "__MET"]->fill(MET);
            _h[channel + "__max_lep_pt_MET"]->fill(max(lepton.Et() / GeV, MET));

            // fill histograms with MET > 75 GeV
            if (MET > 75)
            {
                _h[channel + "__75MET__lep_pt"]->fill(lepton.Et() / GeV);
                _h[channel + "__75MET__W_pt"]->fill(w_pt);
                _h[channel + "__75MET__mT"]->fill(mT);
                _h[channel + "__75MET__MET"]->fill(MET);
                _h[channel + "__75MET__max_lep_pt_MET"]->fill(max(lepton.Et() / GeV, MET));
            }
        }

        void finalize()
        {
            const double scalefactor_fb = crossSection() / sumOfWeights() / femtobarn;
            const double scalefactor_pb = crossSection() / sumOfWeights() / picobarn;

            for (auto &hit : _h)
            {
                if (hit.first.find("_fb") != string::npos)
                {
                    scale(hit.second, scalefactor_fb);
                }
                else
                {
                    scale(hit.second, scalefactor_pb);
                }
            }
        }

    private:
        map<string, Histo1DPtr> _h;
    };

    // The hook for the plugin system
    DECLARE_RIVET_PLUGIN(WJets_test);
}
