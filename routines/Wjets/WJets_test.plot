BEGIN PLOT /WJets_test/Had__lep_pt
Title="TauHad"
XLabel=lep_pt [GeV]
YLabel=$\text{d}\sigma / \text{d} lep_pt$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Had__W_pt
Title="TauHad"
XLabel=W_pt [GeV]
YLabel=$\text{d}\sigma / \text{d} W_pt$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Had__mT
Title="TauHad"
XLabel=mT [GeV]
YLabel=$\text{d}\sigma / \text{d} mT$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Had__MET
Title="TauHad"
XLabel=MET [GeV]
YLabel=$\text{d}\sigma / \text{d} MET$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Had__max_lep_pt_MET
Title="TauHad"
XLabel=max(lep_pt, MET) [GeV]
YLabel=$\text{d}\sigma / \text{d} max(lep_pt, MET)$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Lep__lep_pt
Title="TauLep"
XLabel=lep_pt [GeV]
YLabel=$\text{d}\sigma / \text{d} lep_pt$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Lep__W_pt
Title="TauLep"
XLabel=W_pt [GeV]
YLabel=$\text{d}\sigma / \text{d} W_pt$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Lep__mT
Title="TauLep"
XLabel=mT [GeV]
YLabel=$\text{d}\sigma / \text{d} mT$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Lep__MET
Title="TauLep"
XLabel=MET [GeV]
YLabel=$\text{d}\sigma / \text{d} MET$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Lep__max_lep_pt_MET
Title="TauLep"
XLabel=max(lep_pt, MET) [GeV]
YLabel=$\text{d}\sigma / \text{d} max(lep_pt, MET)$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Had__75MET__lep_pt
Title="TauHad; MET $>$ 75 GeV"
XLabel=lep_pt [GeV]
YLabel=$\text{d}\sigma / \text{d} lep_pt$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Had__75MET__W_pt
Title="TauHad; MET $>$ 75 GeV"
XLabel=W_pt [GeV]
YLabel=$\text{d}\sigma / \text{d} W_pt$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Had__75MET__mT
Title="TauHad; MET $>$ 75 GeV"
XLabel=mT [GeV]
YLabel=$\text{d}\sigma / \text{d} mT$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Had__75MET__MET
Title="TauHad; MET $>$ 75 GeV"
XLabel=MET [GeV]
YLabel=$\text{d}\sigma / \text{d} MET$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Had__75MET__max_lep_pt_MET
Title="TauHad; MET $>$ 75 GeV"
XLabel=max(lep_pt, MET) [GeV]
YLabel=$\text{d}\sigma / \text{d} max(lep_pt, MET)$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Lep__75MET__lep_pt
Title="TauLep; MET $>$ 75 GeV"
XLabel=lep_pt [GeV]
YLabel=$\text{d}\sigma / \text{d} lep_pt$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Lep__75MET__W_pt
Title="TauLep; MET $>$ 75 GeV"
XLabel=W_pt [GeV]
YLabel=$\text{d}\sigma / \text{d} W_pt$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Lep__75MET__mT
Title="TauLep; MET $>$ 75 GeV"
XLabel=mT [GeV]
YLabel=$\text{d}\sigma / \text{d} mT$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Lep__75MET__MET
Title="TauLep; MET $>$ 75 GeV"
XLabel=MET [GeV]
YLabel=$\text{d}\sigma / \text{d} MET$ [fb/GeV]
FullRange=1
END PLOT

BEGIN PLOT /WJets_test/Lep__75MET__max_lep_pt_MET
Title="TauLep; MET $>$ 75 GeV"
XLabel=max(lep_pt, MET) [GeV]
YLabel=$\text{d}\sigma / \text{d} max(lep_pt, MET)$ [fb/GeV]
FullRange=1
END PLOT
