// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/HeavyHadrons.hh"
#include "Rivet/Projections/InvisibleFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"

// HepMC stuff
#include "HepMC/GenParticle.h"
#include "HepMC/GenEvent.h"
#include "HepMC/GenVertex.h"
#include "HepMC/Units.h"

using namespace HepMC;

namespace Rivet {

  /// @brief Z + b(b) in pp at 13 TeV
  class VHF_TEST : public Analysis {
  public:
    /// @name Constructors etc.
    //@{
    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(VHF_TEST);
    //@}

    /// Book histograms and initialise projections before the run
    void init() {

      const FinalState fs;

      // Hadrons
      declare(HeavyHadrons(), "HFHadrons");

      // Photons
      FinalState photons(Cuts::abspid == PID::PHOTON);

      // Muons
      PromptFinalState bare_mu(Cuts::abspid == PID::MUON, true);
      DressedLeptons all_dressed_mu(photons, bare_mu, 0.1, Cuts::open(), true);
      declare(all_dressed_mu, "MuonFinder");

      // Electrons
      PromptFinalState bare_el(Cuts::abspid == PID::ELECTRON, true);
      DressedLeptons all_dressed_el(photons, bare_el, 0.1, Cuts::open(), true);
      declare(all_dressed_el, "ElectronFinder");

      // Jet forming
      VetoedFinalState vfs(FinalState(Cuts::abseta < 4.5));
      vfs.addVetoOnThisFinalState(all_dressed_el);
      vfs.addVetoOnThisFinalState(all_dressed_mu);

      // Construct jets
      FastJets jets(vfs, FastJets::ANTIKT, 0.4, JetAlg::Muons::ALL, JetAlg::Invisibles::DECAY);
      declare(jets, "jets");

      // Get neutrinos for MET calculation
      declare(InvisibleFinalState(true, true), "Neutrinos"); // true, true = only allow prompt invisibles and allow tau decays

      // inclusive
      book(_h["inclusive_nBJets"], "inclusive_nBJets", 5, 0, 5);
      book(_h["inclusive_ZpT"], "inclusive_ZpT", 100, 0, 1000);
      book(_h["inclusive_Zm"], "inclusive_Zm", 100, 0, 200);
      book(_h["inclusive_Lep1pT"], "inclusive_Lep1pT", 30, 0, 300);
      book(_h["inclusive_Lep2pT"], "inclusive_Lep2pT", 30, 0, 300);
      book(_h["inclusive_MET"], "inclusive_MET", 50, 0, 500);

      book(_h["i0b_nBJets"], "i0b_nBJets", 5, 0, 5);
      book(_h["i0b_ZpT"], "i0b_ZpT", 100, 0, 1000);
      book(_h["i0b_Zm"], "i0b_Zm", 100, 0, 200);
      book(_h["i0b_Lep1pT"], "i0b_Lep1pT", 30, 0, 300);
      book(_h["i0b_Lep2pT"], "i0b_Lep2pT", 30, 0, 300);
      book(_h["i0b_MET"], "i0b_MET", 50, 0, 500);

      book(_h["i1b_nBJets"], "i1b_nBJets", 5, 0, 5);
      book(_h["i1b_ZpT"], "i1b_ZpT", 100, 0, 1000);
      book(_h["i1b_Zm"], "i1b_Zm", 100, 0, 200);
      book(_h["i1b_Lep1pT"], "i1b_Lep1pT", 30, 0, 300);
      book(_h["i1b_Lep2pT"], "i1b_Lep2pT", 30, 0, 300);
      book(_h["i1b_MET"], "i1b_MET", 50, 0, 500);

      book(_h["i2b_nBJets"], "i2b_nBJets", 5, 0, 5);
      book(_h["i2b_ZpT"], "i2b_ZpT", 100, 0, 1000);
      book(_h["i2b_Zm"], "i2b_Zm", 100, 0, 200);
      book(_h["i2b_Lep1pT"], "i2b_Lep1pT", 30, 0, 300);
      book(_h["i2b_Lep2pT"], "i2b_Lep2pT", 30, 0, 300);
      book(_h["i2b_MET"], "i2b_MET", 50, 0, 500);
      book(_h["i2b_dRbb"], "i2b_dRbb", 60, 0, 6);

      // 2-tag histograms
      book(_h["i2b_75_150_ZpT"], "i2b_75_150_ZpT", 15, 0, 150);
      book(_h["i2b_150_250_ZpT"], "i2b_150_250_ZpT", 10, 150, 250);
      book(_h["i2b_250_400_ZpT"], "i2b_250_400_ZpT", 15, 250, 400);
      book(_h["i2b_400_ZpT"], "i2b_400_ZpT", 60, 400, 1000);
      book(_h["i2b_75_150_dRbb"], "i2b_75_150_dRbb", 60, 0, 6);
      book(_h["i2b_150_250_dRbb"], "i2b_150_250_dRbb", 60, 0, 6);
      book(_h["i2b_250_400_dRbb"], "i2b_250_400_dRbb", 60, 0, 6);
      book(_h["i2b_400_dRbb"], "i2b_400_dRbb", 60, 0, 6);

      book(_h["i2b_2j_75_150_ZpT"], "i2b_2j_75_150_ZpT", 15, 0, 150);
      book(_h["i2b_2j_150_250_ZpT"], "i2b_2j_150_250_ZpT", 10, 150, 250);
      book(_h["i2b_2j_250_400_ZpT"], "i2b_2j_250_400_ZpT", 15, 250, 400);
      book(_h["i2b_2j_400_ZpT"], "i2b_2j_400_ZpT", 60, 400, 1000);
      book(_h["i2b_2j_75_150_dRbb"], "i2b_2j_75_150_dRbb", 60, 0, 6);
      book(_h["i2b_2j_150_250_dRbb"], "i2b_2j_150_250_dRbb", 60, 0, 6);
      book(_h["i2b_2j_250_400_dRbb"], "i2b_2j_250_400_dRbb", 60, 0, 6);
      book(_h["i2b_2j_400_dRbb"], "i2b_2j_400_dRbb", 60, 0, 6);

      book(_h["i2b_3j_75_150_ZpT"], "i2b_3j_75_150_ZpT", 15, 0, 150);
      book(_h["i2b_3j_150_250_ZpT"], "i2b_3j_150_250_ZpT", 10, 150, 250);
      book(_h["i2b_3j_250_400_ZpT"], "i2b_3j_250_400_ZpT", 15, 250, 400);
      book(_h["i2b_3j_400_ZpT"], "i2b_3j_400_ZpT", 60, 400, 1000);
      book(_h["i2b_3j_75_150_dRbb"], "i2b_3j_75_150_dRbb", 60, 0, 6);
      book(_h["i2b_3j_150_250_dRbb"], "i2b_3j_150_250_dRbb", 60, 0, 6);
      book(_h["i2b_3j_250_400_dRbb"], "i2b_3j_250_400_dRbb", 60, 0, 6);
      book(_h["i2b_3j_400_dRbb"], "i2b_3j_400_dRbb", 60, 0, 6);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Perform the per-event analysis
    void analyze(const Event& event) {

      FourMomentum boson;

      // Access muons from the existing object all_dressed_mu
      const Particles& muons = apply<DressedLeptons>(event, "MuonFinder").particlesByPt();

      // Access electrons from the existing object all_dressed_el
      const Particles& electrons = apply<DressedLeptons>(event, "ElectronFinder").particlesByPt();

      // Veto
      if ( (electrons.size() + muons.size()) == 0 || (electrons.size() + muons.size()) > 2)  vetoEvent;

      // calulate MET and mT
      const Particles &invisibles = apply<InvisibleFinalState>(event, "Neutrinos").particles();
      FourMomentum pMET = sum(invisibles, Kin::mom, FourMomentum()).setZ(0);

      // leptons
      FourMomentum p1;
      FourMomentum p2;

      if ( electrons.size()==2 && muons.size()==0) {
        p1 = electrons[0].momentum();
        p2 = electrons[1].momentum();
        if (p1.pt()/GeV < 30. || p2.pt()/GeV < 30.) vetoEvent;
        if (abs(p1.eta()) > 2.5 || abs(p2.eta()) > 2.5) vetoEvent;
        boson = p1 + p2;
        if (boson.mass()/GeV < 81 || boson.mass()/GeV >= 101) vetoEvent;
      } else if (muons.size()==2 && electrons.size()==0) {
        p1 = muons[0].momentum();
        p2 = muons[1].momentum();
        if (p1.pt()/GeV < 30. || p2.pt()/GeV < 30.) vetoEvent;
        if (abs(p1.eta()) > 2.5 || abs(p2.eta()) > 2.5) vetoEvent;
        boson = p1 + p2;
        if (boson.mass()/GeV < 81 || boson.mass()/GeV >= 101) vetoEvent;
      } else if (electrons.size()==1 && muons.size()==0) {
        p1 = electrons[0].momentum();
        p1.setZ(0);
        if (p1.pt()/GeV < 30.) vetoEvent;
        if (abs(p1.eta()) > 2.5) vetoEvent;
        boson = p1 + pMET;
      } else if (muons.size()==1 && electrons.size()==0) {
        p1 = muons[0].momentum();
        p1.setZ(0);
        if (p1.pt()/GeV < 30.) vetoEvent;
        if (abs(p1.eta()) > 2.5) vetoEvent;
        boson = p1 + pMET;
      }

      Jets jets = apply<JetAlg>(event, "jets").jetsByPt(Cuts::pT>20*GeV && Cuts::absrap < 2.5);
      idiscardIfAnyDeltaRLess(jets, electrons, 0.4);
      idiscardIfAnyDeltaRLess(jets, muons, 0.4);

      Jets btagged;
      const Particles allBs = apply<HeavyHadrons>(event, "HFHadrons").bHadrons(5.0*GeV);
      Particles matchedBs;

      for (const Jet& j : jets) {
        Jet closest_j;
        Particle closest_b;
        double minDR_j_b = 10;

        for (const Particle& bHad : allBs) {
          bool alreadyMatched = false;
          for (const Particle& bMatched : matchedBs) {
            alreadyMatched |= bMatched.isSame(bHad);
          }
          if(alreadyMatched)  continue;

          double DR_j_b = deltaR(j, bHad);
          if ( DR_j_b <= 0.3 && DR_j_b < minDR_j_b) {
            minDR_j_b = DR_j_b;
            closest_j = j;
            closest_b = bHad;
          }
        }

        if(minDR_j_b < 0.3) {
          btagged += closest_j;
          matchedBs += closest_b;
        }
      }

      //size_t njets = jets.size();
      size_t ntags = btagged.size();

      // fill inclusive histograms
      _h["inclusive_nBJets"]->fill(ntags);
      _h["inclusive_ZpT"]->fill(boson.pt()/GeV);
      _h["inclusive_Zm"]->fill(boson.mass()/GeV);
      _h["inclusive_Lep1pT"]->fill(p1.pt()/GeV);
      _h["inclusive_Lep2pT"]->fill(p2.pt()/GeV);
      _h["inclusive_MET"]->fill(pMET.pt()/GeV);

      // boson pT
      double Vpt = boson.pt();

      if (ntags == 0) {
        _h["i0b_nBJets"]->fill(ntags);
        _h["i0b_ZpT"]->fill(Vpt/GeV);
        _h["i0b_Zm"]->fill(boson.mass()/GeV);
        _h["i0b_MET"]->fill(pMET.pt()/GeV);
        _h["i0b_Lep1pT"]->fill(p1.pt()/GeV);
        _h["i0b_Lep2pT"]->fill(p2.pt()/GeV);
      }
      else if (ntags == 1) {
        _h["i1b_nBJets"]->fill(ntags);
        _h["i1b_ZpT"]->fill(Vpt/GeV);
        _h["i1b_Zm"]->fill(boson.mass()/GeV);
        _h["i1b_MET"]->fill(pMET.pt()/GeV);
        _h["i1b_Lep1pT"]->fill(p1.pt()/GeV);
        _h["i1b_Lep2pT"]->fill(p2.pt()/GeV);
      }
      else if ( ntags==2 ) {
        double dRbb = deltaR(btagged[0], btagged[1]);
        double dYbb   = fabs(btagged[0].rap() - btagged[1].rap());
        double dPhibb = deltaPhi(btagged[0], btagged[1]);
        double Mbb    = (btagged[0].mom() + btagged[1].mom()).mass()/GeV;
        double Ptbb   = (btagged[0].mom() + btagged[1].mom()).pt()/GeV;

        _h["i2b_nBJets"]->fill(ntags);
        _h["i2b_ZpT"]->fill(Vpt/GeV);
        _h["i2b_Zm"]->fill(boson.mass()/GeV);
        _h["i2b_MET"]->fill(pMET.pt()/GeV);
        _h["i2b_Lep1pT"]->fill(p1.pt()/GeV);
        _h["i2b_Lep2pT"]->fill(p2.pt()/GeV);
        _h["i2b_dRbb"]->fill(dRbb);

        if (Vpt/GeV >= 75 && Vpt/GeV < 150) {
          _h["i2b_75_150_ZpT"]->fill(Vpt/GeV);
          _h["i2b_75_150_dRbb"]->fill(dRbb);
        } else if (Vpt/GeV >= 150 && Vpt/GeV < 250) {
          _h["i2b_150_250_ZpT"]->fill(Vpt/GeV);
          _h["i2b_150_250_dRbb"]->fill(dRbb);
        } else if (Vpt/GeV >= 250 && Vpt/GeV < 400) {
          _h["i2b_250_400_ZpT"]->fill(Vpt/GeV);
          _h["i2b_250_400_dRbb"]->fill(dRbb);
        } else if (Vpt/GeV >= 400) {
          _h["i2b_400_ZpT"]->fill(Vpt/GeV);
          _h["i2b_400_dRbb"]->fill(dRbb);
        }

        if (jets.size() == 2) {
          if (Vpt/GeV >= 75 && Vpt/GeV < 150) {
            _h["i2b_2j_75_150_ZpT"]->fill(Vpt/GeV);
            _h["i2b_2j_75_150_dRbb"]->fill(dRbb);
          } else if (Vpt/GeV >= 150 && Vpt/GeV < 250) {
            _h["i2b_2j_150_250_ZpT"]->fill(Vpt/GeV);
            _h["i2b_2j_150_250_dRbb"]->fill(dRbb);
          } else if (Vpt/GeV >= 250 && Vpt/GeV < 400) {
            _h["i2b_2j_250_400_ZpT"]->fill(Vpt/GeV);
            _h["i2b_2j_250_400_dRbb"]->fill(dRbb);
          } else if (Vpt/GeV >= 400) {
            _h["i2b_2j_400_ZpT"]->fill(Vpt/GeV);
            _h["i2b_2j_400_dRbb"]->fill(dRbb);
          }
        }
        else if (jets.size() == 3) {
          if (Vpt/GeV >= 75 && Vpt/GeV < 150) {
            _h["i2b_3j_75_150_ZpT"]->fill(Vpt/GeV);
            _h["i2b_3j_75_150_dRbb"]->fill(dRbb);
          } else if (Vpt/GeV >= 150 && Vpt/GeV < 250) {
            _h["i2b_3j_150_250_ZpT"]->fill(Vpt/GeV);
            _h["i2b_3j_150_250_dRbb"]->fill(dRbb);
          } else if (Vpt/GeV >= 250 && Vpt/GeV < 400) {
            _h["i2b_3j_250_400_ZpT"]->fill(Vpt/GeV);
            _h["i2b_3j_250_400_dRbb"]->fill(dRbb);
          } else if (Vpt/GeV >= 400) {
            _h["i2b_3j_400_ZpT"]->fill(Vpt/GeV);
            _h["i2b_3j_400_dRbb"]->fill(dRbb);
          }
        }
      }
    }


    void finalize() {
      scale(_h, crossSectionPerEvent());
    }

    protected:

      size_t _mode;

    private:

      map<string, Histo1DPtr> _h;

  };

  DECLARE_RIVET_PLUGIN(VHF_TEST);
}
