BEGIN PLOT /VHF_TEST/i2b_75_150_ZpT
Title=2-tag; $75 GeV < p_{T}(V) \leq 150 GeV$
XLabel=$p_{T}(V)$
YLabel=$d\sigma/dp_{T}(V)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
END PLOT

BEGIN PLOT /VHF_TEST/i2b_150_250_ZpT
Title=2-tag; $150 GeV < p_{T}(V) \leq 250 GeV$
XLabel=$p_{T}(V)$
YLabel=$d\sigma/dp_{T}(V)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
END PLOT

BEGIN PLOT /VHF_TEST/i2b_250_400_ZpT
Title=2-tag; $250 GeV < p_{T}(V) \leq 400 GeV$
XLabel=$p_{T}(V)$
YLabel=$d\sigma/dp_{T}(V)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
END PLOT

BEGIN PLOT /VHF_TEST/i2b_400_ZpT
Title=2-tag; $400 GeV < p_{T}(V)$
XLabel=$p_{T}(V)$
YLabel=$d\sigma/dp_{T}(V)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
END PLOT

BEGIN PLOT /VHF_TEST/i2b_75_150_dRbb
Title=2-tag; $75 GeV < p_{T}(V) \leq 150 GeV$
XLabel=$\Delta R(bb)$
YLabel=$d\sigma/d\Delta R(bb)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
END PLOT

BEGIN PLOT /VHF_TEST/i2b_150_250_dRbb
Title=2-tag; $150 GeV < p_{T}(V) \leq 250 GeV$
XLabel=$\Delta R(bb)$
YLabel=$d\sigma/d\Delta R(bb)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
END PLOT

BEGIN PLOT /VHF_TEST/i2b_250_400_dRbb
Title=2-tag; $250 GeV < p_{T}(V) \leq 400 GeV$
XLabel=$\Delta R(bb)$
YLabel=$d\sigma/d\Delta R(bb)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
END PLOT

BEGIN PLOT /VHF_TEST/i2b_400_dRbb
Title=2-tag; $400 GeV < p_{T}(V)$
XLabel=$\Delta R(bb)$
YLabel=$d\sigma/d\Delta R(bb)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
END PLOT

BEGIN PLOT /VHF_TEST/i0b_ZpT
Title=0-tag
XLabel=$p_{T}(V)$
YLabel=$d\sigma/dp_{T}(V)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
FullRange=0
XMin=0
XMax=400
END PLOT

BEGIN PLOT /VHF_TEST/i0b_Zm
Title=0-tag
XLabel=$m(V)$
YLabel=$d\sigma/dm(V)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
END PLOT

BEGIN PLOT /VHF_TEST/i0b_nBJets
Title=0-tag
XLabel=N($b$-jets)
YLabel=$d\sigma/dN$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
END PLOT

BEGIN PLOT /VHF_TEST/i1b_ZpT
Title=1-tag
XLabel=$p_{T}(V)$
YLabel=$d\sigma/dp_{T}(V)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
FullRange=0
XMin=0
XMax=400
END PLOT

BEGIN PLOT /VHF_TEST/i1b_Zm
Title=1-tag
XLabel=$m(V)$
YLabel=$d\sigma/dm(V)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
END PLOT

BEGIN PLOT /VHF_TEST/i1b_nBJets
Title=1-tag
XLabel=N($b$-jets)
YLabel=$d\sigma/dN$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
END PLOT

BEGIN PLOT /VHF_TEST/i2b_ZpT
Title=2-tag
XLabel=$p_{T}(V)$
YLabel=$d\sigma/dp_{T}(V)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
FullRange=0
XMin=0
XMax=400
END PLOT

BEGIN PLOT /VHF_TEST/i2b_Zm
Title=2-tag
XLabel=$m(V)$
YLabel=$d\sigma/dm(V)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
END PLOT

BEGIN PLOT /VHF_TEST/i2b_nBJets
Title=2-tag
XLabel=N($b$-jets)
YLabel=$d\sigma/dN$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
END PLOT

BEGIN PLOT /VHF_TEST/inclusive_nBJets
XLabel=N($b$-jets)
YLabel=$d\sigma/dN$
LogY=1
LogX=0
FullRange=1
NormalizeToIntegral=0
END PLOT

BEGIN PLOT /VHF_TEST/i2b_dRbb
XLabel=$\Delta R(bb)$
YLabel=$d\sigma/d\Delta R(bb)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
END PLOT

BEGIN PLOT /VHF_TEST/inclusive_ZpT
XLabel=$p_{T}(V)$
YLabel=$d\sigma/dp_{T}(V)$
LogY=0
LogX=0
FullRange=0
XMin=0
XMax=400
NormalizeToIntegral=0
Rebin=1
END PLOT

BEGIN PLOT /VHF_TEST/i2b_2j_75_150_dRbb
Title=2-tag; 2-jet; $75 GeV < p_{T}(V) \leq 150 GeV$
XLabel=$\Delta R(bb)$
YLabel=$d\sigma/d\Delta R(bb)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
END PLOT

BEGIN PLOT /VHF_TEST/i2b_2j_150_250_dRbb
Title=2-tag; 2-jet; $150 GeV < p_{T}(V) \leq 250 GeV$
XLabel=$\Delta R(bb)$
YLabel=$d\sigma/d\Delta R(bb)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
END PLOT

BEGIN PLOT /VHF_TEST/i2b_2j_250_400_dRbb
Title=2-tag; 2-jet; $250 GeV < p_{T}(V) \leq 400 GeV$
XLabel=$\Delta R(bb)$
YLabel=$d\sigma/d\Delta R(bb)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
END PLOT

BEGIN PLOT /VHF_TEST/i2b_2j_400_dRbb
Title=2-tag; 2-jet; $400 GeV < p_{T}(V)$
XLabel=$\Delta R(bb)$
YLabel=$d\sigma/d\Delta R(bb)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
END PLOT

BEGIN PLOT /VHF_TEST/i2b_3j_75_150_dRbb
Title=2-tag; 3-jet; $75 GeV < p_{T}(V) \leq 150 GeV$
XLabel=$\Delta R(bb)$
YLabel=$d\sigma/d\Delta R(bb)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
END PLOT

BEGIN PLOT /VHF_TEST/i2b_3j_150_250_dRbb
Title=2-tag; 3-jet; $150 GeV < p_{T}(V) \leq 250 GeV$
XLabel=$\Delta R(bb)$
YLabel=$d\sigma/d\Delta R(bb)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
END PLOT

BEGIN PLOT /VHF_TEST/i2b_3j_250_400_dRbb
Title=2-tag; 3-jet; $250 GeV < p_{T}(V) \leq 400 GeV$
XLabel=$\Delta R(bb)$
YLabel=$d\sigma/d\Delta R(bb)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
END PLOT

BEGIN PLOT /VHF_TEST/i2b_3j_400_dRbb
Title=2-tag; 3-jet; $400 GeV < p_{T}(V)$
XLabel=$\Delta R(bb)$
YLabel=$d\sigma/d\Delta R(bb)$
LogY=0
LogX=0
FullRange=1
NormalizeToIntegral=0
Rebin=1
END PLOT