#!/bin/bash

# grid submision
# mc16_13TeV.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.merge.EVNT.e8351_e7400
# mc16_13TeV.506196.MGPy8EG_Zmumu_FxFx_3jets_HT2bias_BFilter.merge.EVNT.e8382_e7400
# mc16_13TeV.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.merge.EVNT.e8351_e7400
# mc16_13TeV.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.merge.EVNT.e8382_e7400
# mc15_13TeV.700386.Sh_2211_Wmunu_maxHTpTV2_CKKW15.evgen.EVNT.e8338
# mc15_13TeV.700387.Sh_2211_Wmunu_maxHTpTV2_CKKW30.evgen.EVNT.e8338

# mc15_13TeV.700342.Sh_2211_Wmunu_maxHTpTV2_CFilterBVeto.evgen.EVNT.e8351
# mc15_13TeV.700343.Sh_2211_Wmunu_maxHTpTV2_CVetoBVeto.evgen.EVNT.e8351
# mc15_13TeV.508983.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_CFilterBVeto.merge.EVNT.e8382_e7400
# mc15_13TeV.508984.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_CVetoBVeto.evgen.EVNT.e8382

# mc15_13TeV.700095.Sh_2210_Wmunubb_EnhLogpTV.evgen.EVNT.e8118

pathena --extOutFile=MyOutput.yoda.gz \
--inDS=mc16_13TeV.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.merge.EVNT.e8351_e7400 \
--outDS=user.mmuskinj.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.vhf_test.v3 \
--extFile RivetVHF_TEST.so \
--nFilesPerJob 1 \
/afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py

pathena --extOutFile=MyOutput.yoda.gz \
--inDS=mc16_13TeV.506196.MGPy8EG_Zmumu_FxFx_3jets_HT2bias_BFilter.merge.EVNT.e8382_e7400 \
--outDS=user.mmuskinj.506196.MGPy8EG_Zmumu_FxFx_3jets_HT2bias_BFilter.vhf_test.v3 \
--extFile RivetVHF_TEST.so \
--nFilesPerJob 1 \
/afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py

pathena --extOutFile=MyOutput.yoda.gz \
--inDS=mc16_13TeV.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.merge.EVNT.e8351_e7400 \
--outDS=user.mmuskinj.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.vhf_test.v6 \
--extFile RivetVHF_TEST.so \
/afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py
# --nFilesPerJob 1 \

pathena --extOutFile=MyOutput.yoda.gz \
--inDS=mc15_13TeV.700342.Sh_2211_Wmunu_maxHTpTV2_CFilterBVeto.evgen.EVNT.e8351 \
--outDS=user.mmuskinj.700342.Sh_2211_Wmunu_maxHTpTV2_CFilterBVeto.vhf_test.v3 \
--extFile RivetVHF_TEST.so \
--nFilesPerJob 1 \
/afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py

pathena --extOutFile=MyOutput.yoda.gz \
--inDS=mc15_13TeV.700343.Sh_2211_Wmunu_maxHTpTV2_CVetoBVeto.evgen.EVNT.e8351 \
--outDS=user.mmuskinj.700343.Sh_2211_Wmunu_maxHTpTV2_CVetoBVeto.vhf_test.v3 \
--extFile RivetVHF_TEST.so \
--nFilesPerJob 1 \
/afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py

pathena --extOutFile=MyOutput.yoda.gz \
--inDS=mc16_13TeV.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.merge.EVNT.e8382_e7400 \
--outDS=user.mmuskinj.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.vhf_test.v6 \
--extFile RivetVHF_TEST.so \
/afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py
# --nFilesPerJob 1 \

pathena --extOutFile=MyOutput.yoda.gz \
--inDS=mc15_13TeV.508983.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_CFilterBVeto.merge.EVNT.e8382_e7400 \
--outDS=user.mmuskinj.508983.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_CFilterBVeto.vhf_test.v3 \
--extFile RivetVHF_TEST.so \
--nFilesPerJob 1 \
/afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py

pathena --extOutFile=MyOutput.yoda.gz \
--inDS=mc15_13TeV.508984.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_CVetoBVeto.evgen.EVNT.e8382 \
--outDS=user.mmuskinj.508984.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_CVetoBVeto.vhf_test.v3 \
--extFile RivetVHF_TEST.so \
--nFilesPerJob 1 \
/afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py

pathena --extOutFile=MyOutput.yoda.gz \
--inDS=mc15_13TeV.700386.Sh_2211_Wmunu_maxHTpTV2_CKKW15.evgen.EVNT.e8338 \
--outDS=user.mmuskinj.700386.Sh_2211_Wmunu_maxHTpTV2_CKKW15.vhf_test.v3 \
--extFile RivetVHF_TEST.so \
--nFilesPerJob 1 \
/afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py

pathena --extOutFile=MyOutput.yoda.gz \
--inDS=mc15_13TeV.700387.Sh_2211_Wmunu_maxHTpTV2_CKKW30.evgen.EVNT.e8338 \
--outDS=user.mmuskinj.700387.Sh_2211_Wmunu_maxHTpTV2_CKKW30.vhf_test.v3 \
--extFile RivetVHF_TEST.so \
--nFilesPerJob 1 \
/afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py

pathena --extOutFile=MyOutput.yoda.gz \
--inDS=mc15_13TeV.700387.Sh_2211_Wmunu_maxHTpTV2_CKKW30.evgen.EVNT.e8338 \
--outDS=user.mmuskinj.700387.Sh_2211_Wmunu_maxHTpTV2_CKKW30.vhf_test.v3 \
--extFile RivetVHF_TEST.so \
--nFilesPerJob 1 \
/afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py

pathena --extOutFile=MyOutput.yoda.gz \
--inDS=mc15_13TeV.700095.Sh_2210_Wmunubb_EnhLogpTV.evgen.EVNT.e8118 \
--outDS=user.mmuskinj.700095.Sh_2210_Wmunubb_EnhLogpTV.vhf_test.v6 \
--extFile RivetVHF_TEST.so \
/afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py

pathena --extOutFile=MyOutput.yoda.gz \
--inDS=user.mmuskinj.100000.rikkert.EVNT.65_EXT0/ \
--outDS=user.mmuskinj.100000.rikkert.vhf_test.v6.p2 \
--extFile RivetVHF_TEST.so \
/afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py

datasets=("mc15_13TeV:mc15_13TeV.364158.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_BFilter.evgen.EVNT.e5340" "mc15_13TeV:mc15_13TeV.364161.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_BFilter.evgen.EVNT.e5340" "mc15_13TeV:mc15_13TeV.364164.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_BFilter.evgen.EVNT.e5340" "mc15_13TeV:mc15_13TeV.364167.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_BFilter.evgen.EVNT.e5340" "mc15_13TeV:mc15_13TeV.364168.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV500_1000.evgen.EVNT.e5340" "mc15_13TeV:mc15_13TeV.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS.evgen.EVNT.e5340")
for dataset in $datasets; do
    outds=${dataset:11}
    outds=${outds:0: -17}
    pathena --extOutFile=MyOutput.yoda.gz \
    --inDS=$dataset \
    --outDS=user.mmuskinj.$outds.vhf_test.v6 \
    --extFile RivetVHF_TEST.so \
    /afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py
done

datasets=("mc15_13TeV.363626.MGPy8EG_N30NLO_Wmunu_Ht0_70_BFilter.evgen.EVNT.e4944" "mc15_13TeV.363629.MGPy8EG_N30NLO_Wmunu_Ht70_140_BFilter.evgen.EVNT.e4944" "mc15_13TeV.363632.MGPy8EG_N30NLO_Wmunu_Ht140_280_BFilter.evgen.EVNT.e4944" "mc15_13TeV.363635.MGPy8EG_N30NLO_Wmunu_Ht280_500_BFilter.evgen.EVNT.e4944" "mc15_13TeV.363638.MGPy8EG_N30NLO_Wmunu_Ht500_700_BFilter.evgen.EVNT.e4944" "mc15_13TeV.363641.MGPy8EG_N30NLO_Wmunu_Ht700_1000_BFilter.evgen.EVNT.e4944" "mc15_13TeV.363644.MGPy8EG_N30NLO_Wmunu_Ht1000_2000_BFilter.evgen.EVNT.e4944" "mc15_13TeV.363647.MGPy8EG_N30NLO_Wmunu_Ht2000_E_CMS_BFilter.evgen.EVNT.e4944")
for dataset in $datasets; do
    outds=${dataset:11}
    outds=${outds:0: -17}
    pathena --extOutFile=MyOutput.yoda.gz \
    --inDS=$dataset \
    --outDS=user.mmuskinj.$outds.vhf_test.v6 \
    --extFile RivetVHF_TEST.so \
    --nFilesPerJob 10 \
    /afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py
done

datasets=("mc15_13TeV:mc15_13TeV.363624.MGPy8EG_N30NLO_Wmunu_Ht0_70_CVetoBVeto.evgen.EVNT.e4944" "mc15_13TeV:mc15_13TeV.363625.MGPy8EG_N30NLO_Wmunu_Ht0_70_CFilterBVeto.evgen.EVNT.e4944" "mc15_13TeV:mc15_13TeV.363627.MGPy8EG_N30NLO_Wmunu_Ht70_140_CVetoBVeto.evgen.EVNT.e4944" "mc15_13TeV:mc15_13TeV.363628.MGPy8EG_N30NLO_Wmunu_Ht70_140_CFilterBVeto.evgen.EVNT.e4944" "mc15_13TeV:mc15_13TeV.363630.MGPy8EG_N30NLO_Wmunu_Ht140_280_CVetoBVeto.evgen.EVNT.e4944" "mc15_13TeV:mc15_13TeV.363631.MGPy8EG_N30NLO_Wmunu_Ht140_280_CFilterBVeto.evgen.EVNT.e4944" "mc15_13TeV:mc15_13TeV.363633.MGPy8EG_N30NLO_Wmunu_Ht280_500_CVetoBVeto.evgen.EVNT.e4944" "mc15_13TeV:mc15_13TeV.363634.MGPy8EG_N30NLO_Wmunu_Ht280_500_CFilterBVeto.evgen.EVNT.e4944" "mc15_13TeV:mc15_13TeV.363636.MGPy8EG_N30NLO_Wmunu_Ht500_700_CVetoBVeto.evgen.EVNT.e4944" "mc15_13TeV:mc15_13TeV.363637.MGPy8EG_N30NLO_Wmunu_Ht500_700_CFilterBVeto.evgen.EVNT.e4944" "mc15_13TeV:mc15_13TeV.363639.MGPy8EG_N30NLO_Wmunu_Ht700_1000_CVetoBVeto.evgen.EVNT.e4944" "mc15_13TeV:mc15_13TeV.363640.MGPy8EG_N30NLO_Wmunu_Ht700_1000_CFilterBVeto.evgen.EVNT.e4944" "mc15_13TeV:mc15_13TeV.363642.MGPy8EG_N30NLO_Wmunu_Ht1000_2000_CVetoBVeto.evgen.EVNT.e4944" "mc15_13TeV:mc15_13TeV.363643.MGPy8EG_N30NLO_Wmunu_Ht1000_2000_CFilterBVeto.evgen.EVNT.e4944")
for dataset in $datasets; do
    outds=${dataset:11}
    outds=${outds:0: -17}
    pathena --extOutFile=MyOutput.yoda.gz \
    --inDS=$dataset \
    --outDS=user.mmuskinj.$outds.vhf_test.v3 \
    --extFile RivetVHF_TEST.so \
    --nFilesPerJob 1 \
    /afs/f9.ijs.si/home/miham/git/wb-rivet-tests/share/grid_jo.py
done

# Merge grid output
rivet-merge -e -o /data0/miham/data/Yoda/user.mmuskinj.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.vhf_test.v2_EXT0.yoda.gz /data0/miham/data/Yoda/user.mmuskinj.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.vhf_test.v2_EXT0/*
rivet-merge -e -o /data0/miham/data/Yoda/user.mmuskinj.506196.MGPy8EG_Zmumu_FxFx_3jets_HT2bias_BFilter.vhf_test.v2_EXT0.yoda.gz /data0/miham/data/Yoda/user.mmuskinj.506196.MGPy8EG_Zmumu_FxFx_3jets_HT2bias_BFilter.vhf_test.v2_EXT0/*
rivet-merge -e -o /data0/miham/data/Yoda/user.mmuskinj.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.vhf_test.v2_EXT0.yoda.gz /data0/miham/data/Yoda/user.mmuskinj.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.vhf_test.v2_EXT0/*
rivet-merge -e -o /data0/miham/data/Yoda/user.mmuskinj.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.vhf_test.v2_EXT0.yoda.gz /data0/miham/data/Yoda/user.mmuskinj.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.vhf_test.v2_EXT0/*

for file in `ls /data0/miham/data/Yoda/vhf_test.v6`; do
    rivet-merge -e -o /data0/miham/data/Yoda/vhf_test.v6/$file.yoda.gz /data0/miham/data/Yoda/vhf_test.v6/$file/*
done

# Analysis path
export RIVET_ANALYSIS_PATH=/global/homes/m/mmuskinj/git/wb-rivet-tests/routines/Vhf
# scale to cross section
# 700323    Sh_2211_Zmumu_maxHTpTV2_BFilter               e8351    285200000    2221.3  * 2.439439E-02 = 54.187259
# 506196    MGPy8EG_Zmumu_FxFx_3jets_HT2bias_BFilter      e8382    121098000    2221.3  * 2.142405E-02 = 47.589242
# 700341    Sh_2211_Wmunu_maxHTpTV2_BFilter               e8351    285385000    21806.0 * 0.0097968    = 213.62902
# 700342    Sh_2211_Wmunu_maxHTpTV2_CFilterBVeto          e8351    607460000    21806.0 * 0.1460112    = 3183.92
# 700343    Sh_2211_Wmunu_maxHTpTV2_CVetoBVeto            e8351    616000000    21806.0 * 8.435538E-01 = 18394.5
# 508982    MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter      e8382    188415000    21806.0 * 7.507885E-03 = 163.71694
# 508983    MGPy8EG_Wmunu_FxFx_3jets_HT2bias_CFilterBVeto e8382    250590000    22200.0 * 1.533110E-01 = 3403.50
# 508984    MGPy8EG_Wmunu_FxFx_3jets_HT2bias_CVetoBVeto   e8382    377190000    22117.0 * 8.390125E-01 = 18556.4

# 700323    Sh_2211_Zmumu_maxHTpTV2_BFilter             2221.3  * 1.0  * 2.439439E-02 = 54.187259
# 506196    MGPy8EG_Zmumu_FxFx_3jets_HT2bias_BFilter    2273.8  * 1.0  * 2.142405E-02 = 48.714005
# 700341    Sh_2211_Wmunu_maxHTpTV2_BFilter             21806.0 * 1.0  * 0.0097968    = 213.62902
# 508982    MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter    22173.0 * 1.0  * 7.507885E-03 = 166.47233
# 700386    Sh_2211_Wmunu_maxHTpTV2_CKKW15              21826.0 * 1.0  * 1.000000E+00 = 21826.000
# 700387    Sh_2211_Wmunu_maxHTpTV2_CKKW30              21877.0 * 1.0  * 1.000000E+00 = 21877.000
# 363626    MGPy8EG_N30NLO_Wmunu_Ht0_70_BFilter         16712.0 * 1.12 * 2.4676E-02   = 461.87155
# 363629    MGPy8EG_N30NLO_Wmunu_Ht70_140_BFilter       755.72  * 1.12 * 5.0391E-02   = 42.651265
# 363632    MGPy8EG_N30NLO_Wmunu_Ht140_280_BFilter      319.49  * 1.12 * 7.3350E-02   = 26.246742
# 363635    MGPy8EG_N30NLO_Wmunu_Ht280_500_BFilter      73.593  * 1.12 * 9.7299E-02   = 8.0197883
# 363638    MGPy8EG_N30NLO_Wmunu_Ht500_700_BFilter      11.515  * 1.12 * 1.1657E-01   = 1.5033800
# 363641    MGPy8EG_N30NLO_Wmunu_Ht700_1000_BFilter     4.0164  * 1.12 * 1.3022E-01   = 0.5857775
# 363644    MGPy8EG_N30NLO_Wmunu_Ht1000_2000_BFilter    1.3287  * 1.12 * 1.4496E-01   = 0.2157214

# 363624    MGPy8EG_N30NLO_Wmunu_Ht0_70_CVetoBVeto         16720.0  * 1.12 * 8.3759E-01 = 15685.0
# 363625    MGPy8EG_N30NLO_Wmunu_Ht0_70_CFilterBVeto       16719.0  * 1.12 * 1.3895E-01 = 2601.88
# 363626    MGPy8EG_N30NLO_Wmunu_Ht0_70_BFilter            16712.0  * 1.12 * 2.4676E-02 = 461.872
# 363627    MGPy8EG_N30NLO_Wmunu_Ht70_140_CVetoBVeto       755.37   * 1.12 * 7.1230E-01 = 602.616
# 363628    MGPy8EG_N30NLO_Wmunu_Ht70_140_CFilterBVeto     755.79   * 1.12 * 2.4174E-01 = 204.629
# 363629    MGPy8EG_N30NLO_Wmunu_Ht70_140_BFilter          755.72   * 1.12 * 5.0391E-02 = 42.6513
# 363630    MGPy8EG_N30NLO_Wmunu_Ht140_280_CVetoBVeto      318.8    * 1.12 * 6.6601E-01 = 237.803
# 363631    MGPy8EG_N30NLO_Wmunu_Ht140_280_CFilterBVeto    320.03   * 1.12 * 2.6821E-01 = 96.1355
# 363632    MGPy8EG_N30NLO_Wmunu_Ht140_280_BFilter         319.49   * 1.12 * 7.3350E-02 = 26.2467
# 363633    MGPy8EG_N30NLO_Wmunu_Ht280_500_CVetoBVeto      73.567   * 1.12 * 6.1915E-01 = 51.0149
# 363634    MGPy8EG_N30NLO_Wmunu_Ht280_500_CFilterBVeto    73.564   * 1.12 * 2.8642E-01 = 23.5986
# 363635    MGPy8EG_N30NLO_Wmunu_Ht280_500_BFilter         73.593   * 1.12 * 9.7299E-02 = 8.01979
# 363636    MGPy8EG_N30NLO_Wmunu_Ht500_700_CVetoBVeto      11.521   * 1.12 * 5.8715E-01 = 7.57630
# 363637    MGPy8EG_N30NLO_Wmunu_Ht500_700_CFilterBVeto    11.519   * 1.12 * 2.9894E-01 = 3.85671
# 363638    MGPy8EG_N30NLO_Wmunu_Ht500_700_BFilter         11.515   * 1.12 * 1.1657E-01 = 1.50338
# 363639    MGPy8EG_N30NLO_Wmunu_Ht700_1000_CVetoBVeto     4.0222   * 1.12 * 5.6691E-01 = 2.55385
# 363640    MGPy8EG_N30NLO_Wmunu_Ht700_1000_CFilterBVeto   4.0238   * 1.12 * 3.0560E-01 = 1.37723
# 363641    MGPy8EG_N30NLO_Wmunu_Ht700_1000_BFilter        4.0164   * 1.12 * 1.3022E-01 = 0.585777
# 363642    MGPy8EG_N30NLO_Wmunu_Ht1000_2000_CVetoBVeto    1.3262   * 1.12 * 5.4585E-01 = 0.810775
# 363643    MGPy8EG_N30NLO_Wmunu_Ht1000_2000_CFilterBVeto  1.3239   * 1.12 * 3.1089E-01 = 0.460978
# 363644    MGPy8EG_N30NLO_Wmunu_Ht1000_2000_BFilter       1.3287   * 1.12 * 1.4496E-01 = 0.215721
# 363645    MGPy8EG_N30NLO_Wmunu_Ht2000_E_CMS_CVetoBVeto   0.042047 * 1.12 * 5.2270E-01 = 0.0246153
# 363646    MGPy8EG_N30NLO_Wmunu_Ht2000_E_CMS_CFilterBVeto 0.042048 * 1.12 * 3.1749E-01 = 0.0149518
# 363647    MGPy8EG_N30NLO_Wmunu_Ht2000_E_CMS_BFilter      0.042049 * 1.12 * 1.6141E-01 = 0.00760158

# 364172    Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter     19138.0 * 4.4820E-02  = 857.765
# 364175    Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter   945.78  * 0.10341     = 97.8031
# 364178    Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter  339.64  * 0.10898     = 37.0140
# 364181    Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_BFilter  72.091  * 1.3706E-01  = 9.88079
# 364182    Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV500_1000         15.047  * 1.0000E+00  = 15.0470
# 364183    Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV1000_E_CMS       1.2344  * 1.0000E+00  = 1.23440
# 364158    Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_BFilter    19138.0 * 0.9702 * 4.4641E-02 = 828.880
# 364161    Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_BFilter  944.14  * 0.9702 * 8.4708E-02 = 77.5929
# 364164    Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_BFilter 339.64  * 0.9702 * 0.110298   = 36.3453
# 364167    Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_BFilter 72.058  * 0.9702 * 1.2542E-01 = 8.76820
# 364168    Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV500_1000        15.006  * 0.9702 * 1.0000E+00 = 14.5588
# 364169    Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS      1.2348  * 0.9702 * 1.0000E+00 = 1.19800

yodascale -c '.* 166.47233x' user.mmuskinj.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.vhf_test.v6_EXT0.yoda.gz
yodascale -c '.* 213.62902x' user.mmuskinj.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.vhf_test.v6_EXT0.yoda.gz

yodascale -c '.* 54.187259x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 213.62902x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 3183.92x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.700342.Sh_2211_Wmunu_maxHTpTV2_CFilterBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 18394.5x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.700343.Sh_2211_Wmunu_maxHTpTV2_CVetoBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 21826.000x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.700386.Sh_2211_Wmunu_maxHTpTV2_CKKW15.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 21877.000x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.700387.Sh_2211_Wmunu_maxHTpTV2_CKKW30.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 48.714005x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.506196.MGPy8EG_Zmumu_FxFx_3jets_HT2bias_BFilter.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 166.47233x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 3403.50x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.508983.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_CFilterBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 18556.4x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.508984.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_CVetoBVeto.vhf_test.v3_EXT0.yoda.gz

yodascale -c '.* 15685.0x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363624.MGPy8EG_N30NLO_Wmunu_Ht0_70_CVetoBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 2601.88x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363625.MGPy8EG_N30NLO_Wmunu_Ht0_70_CFilterBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 461.872x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.363626.MGPy8EG_N30NLO_Wmunu_Ht0_70_BFilter.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 602.616x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363627.MGPy8EG_N30NLO_Wmunu_Ht70_140_CVetoBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 204.629x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363628.MGPy8EG_N30NLO_Wmunu_Ht70_140_CFilterBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 42.6513x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.363629.MGPy8EG_N30NLO_Wmunu_Ht70_140_BFilter.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 237.803x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363630.MGPy8EG_N30NLO_Wmunu_Ht140_280_CVetoBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 96.1355x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363631.MGPy8EG_N30NLO_Wmunu_Ht140_280_CFilterBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 26.2467x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.363632.MGPy8EG_N30NLO_Wmunu_Ht140_280_BFilter.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 51.0149x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363633.MGPy8EG_N30NLO_Wmunu_Ht280_500_CVetoBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 23.5986x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363634.MGPy8EG_N30NLO_Wmunu_Ht280_500_CFilterBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 8.01979x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.363635.MGPy8EG_N30NLO_Wmunu_Ht280_500_BFilter.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 7.57630x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363636.MGPy8EG_N30NLO_Wmunu_Ht500_700_CVetoBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 3.85671x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363637.MGPy8EG_N30NLO_Wmunu_Ht500_700_CFilterBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 1.50338x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.363638.MGPy8EG_N30NLO_Wmunu_Ht500_700_BFilter.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 2.55385x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363639.MGPy8EG_N30NLO_Wmunu_Ht700_1000_CVetoBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 1.37723x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363640.MGPy8EG_N30NLO_Wmunu_Ht700_1000_CFilterBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 0.585777x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.363641.MGPy8EG_N30NLO_Wmunu_Ht700_1000_BFilter.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 0.810775x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363642.MGPy8EG_N30NLO_Wmunu_Ht1000_2000_CVetoBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 0.460978x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363643.MGPy8EG_N30NLO_Wmunu_Ht1000_2000_CFilterBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 0.215721x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.363644.MGPy8EG_N30NLO_Wmunu_Ht1000_2000_BFilter.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 0.0246153x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363645.MGPy8EG_N30NLO_Wmunu_Ht2000_E_CMS_CVetoBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 0.0149518x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.mc15_13TeV.363646.MGPy8EG_N30NLO_Wmunu_Ht2000_E_CMS_CFilterBVeto.vhf_test.v3_EXT0.yoda.gz
yodascale -c '.* 0.00760158x' /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.363647.MGPy8EG_N30NLO_Wmunu_Ht2000_E_CMS_BFilter.vhf_test.v3_EXT0.yoda.gz

yodascale -c '.* 857.765x' /data0/miham/data/Yoda/vhf_test.v6/user.mmuskinj.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter.vhf_test.v6_EXT0.yoda.gz
yodascale -c '.* 97.8031x' /data0/miham/data/Yoda/vhf_test.v6/user.mmuskinj.364175.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter.vhf_test.v6_EXT0.yoda.gz
yodascale -c '.* 37.0140x' /data0/miham/data/Yoda/vhf_test.v6/user.mmuskinj.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter.vhf_test.v6_EXT0.yoda.gz
yodascale -c '.* 9.88079x' /data0/miham/data/Yoda/vhf_test.v6/user.mmuskinj.364181.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_BFilter.vhf_test.v6_EXT0.yoda.gz
yodascale -c '.* 15.0470x' /data0/miham/data/Yoda/vhf_test.v6/user.mmuskinj.364182.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV500_1000.vhf_test.v6_EXT0.yoda.gz
yodascale -c '.* 1.23440x' /data0/miham/data/Yoda/vhf_test.v6/user.mmuskinj.364183.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV1000_E_CMS.vhf_test.v6_EXT0.yoda.gz

yodascale -c '.* 828.880x' user.mmuskinj.mc15_13TeV.364158.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_BFilter.vhf_test.v6_EXT0.yoda.gz
yodascale -c '.* 77.5929x' user.mmuskinj.mc15_13TeV.364161.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_BFilter.vhf_test.v6_EXT0.yoda.gz
yodascale -c '.* 36.3453x' user.mmuskinj.mc15_13TeV.364164.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_BFilter.vhf_test.v6_EXT0.yoda.gz
yodascale -c '.* 8.76820x' user.mmuskinj.mc15_13TeV.364167.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_BFilter.vhf_test.v6_EXT0.yoda.gz
yodascale -c '.* 14.5588x' user.mmuskinj.mc15_13TeV.364168.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV500_1000.vhf_test.v6_EXT0.yoda.gz
yodascale -c '.* 1.19800x' user.mmuskinj.mc15_13TeV.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS.vhf_test.v6_EXT0.yoda.gz

yodascale -c '.* 85.084x' user.mmuskinj.700095.Sh_2210_Wmunubb_EnhLogpTV.vhf_test.v6_EXT0.yoda.gz

rivet-merge -o user.mmuskinj.3636XX.MGPy8EG_N30NLO_Wmunu.vhf_test.v3.yoda.gz user.mmuskinj.363626.MGPy8EG_N30NLO_Wmunu_Ht0_70_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.363629.MGPy8EG_N30NLO_Wmunu_Ht70_140_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.363632.MGPy8EG_N30NLO_Wmunu_Ht140_280_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.363635.MGPy8EG_N30NLO_Wmunu_Ht280_500_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.363638.MGPy8EG_N30NLO_Wmunu_Ht500_700_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.363641.MGPy8EG_N30NLO_Wmunu_Ht700_1000_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.363644.MGPy8EG_N30NLO_Wmunu_Ht1000_2000_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363624.MGPy8EG_N30NLO_Wmunu_Ht0_70_CVetoBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363625.MGPy8EG_N30NLO_Wmunu_Ht0_70_CFilterBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363627.MGPy8EG_N30NLO_Wmunu_Ht70_140_CVetoBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363628.MGPy8EG_N30NLO_Wmunu_Ht70_140_CFilterBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363630.MGPy8EG_N30NLO_Wmunu_Ht140_280_CVetoBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363631.MGPy8EG_N30NLO_Wmunu_Ht140_280_CFilterBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363633.MGPy8EG_N30NLO_Wmunu_Ht280_500_CVetoBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363634.MGPy8EG_N30NLO_Wmunu_Ht280_500_CFilterBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363636.MGPy8EG_N30NLO_Wmunu_Ht500_700_CVetoBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363637.MGPy8EG_N30NLO_Wmunu_Ht500_700_CFilterBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363639.MGPy8EG_N30NLO_Wmunu_Ht700_1000_CVetoBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363640.MGPy8EG_N30NLO_Wmunu_Ht700_1000_CFilterBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363642.MGPy8EG_N30NLO_Wmunu_Ht1000_2000_CVetoBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.363643.MGPy8EG_N30NLO_Wmunu_Ht1000_2000_CFilterBVeto.vhf_test.v3_EXT0-scaled.yoda.gz

rivet-merge -o user.mmuskinj.mc15_13TeV.364xxx.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV_BFilter.vhf_test.v6_EXT0.yoda.gz user.mmuskinj.mc15_13TeV.364158.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_BFilter.vhf_test.v6_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.364161.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_BFilter.vhf_test.v6_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.364164.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_BFilter.vhf_test.v6_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.364167.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_BFilter.vhf_test.v6_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.364168.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV500_1000.vhf_test.v6_EXT0-scaled.yoda.gz user.mmuskinj.mc15_13TeV.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS.vhf_test.v6_EXT0-scaled.yoda.gz

rivet-merge -o user.mmuskinj.70034X.Sh_2211_Wmunu_maxHTpTV2.vhf_test.v3_EXT0.yoda.gz user.mmuskinj.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.700342.Sh_2211_Wmunu_maxHTpTV2_CFilterBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.700343.Sh_2211_Wmunu_maxHTpTV2_CVetoBVeto.vhf_test.v3_EXT0-scaled.yoda.gz

rivet-merge -o user.mmuskinj.50898X.MGPy8EG_Wmunu_FxFx_3jets_HT2bias.vhf_test.v3_EXT0.yoda.gz user.mmuskinj.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.508983.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_CFilterBVeto.vhf_test.v3_EXT0-scaled.yoda.gz user.mmuskinj.508984.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_CVetoBVeto.vhf_test.v3_EXT0-scaled.yoda.gz

rivet-merge -o user.mmuskinj.3641XX.Sherpa_221_NNPDF30NNLO_Wenu_BFilter.vhf_test.v6_EXT0.yoda.gz user.mmuskinj.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter.vhf_test.v6_EXT0-scaled.yoda.gz user.mmuskinj.364175.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter.vhf_test.v6_EXT0-scaled.yoda.gz user.mmuskinj.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter.vhf_test.v6_EXT0-scaled.yoda.gz user.mmuskinj.364181.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_BFilter.vhf_test.v6_EXT0-scaled.yoda.gz user.mmuskinj.364182.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV500_1000.vhf_test.v6_EXT0-scaled.yoda.gz user.mmuskinj.364183.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV1000_E_CMS.vhf_test.v6_EXT0-scaled.yoda.gz

rivet-mkhtml \
    Sh_2211_Wmunu_maxHTpTV2_BFilter.700341.yoda:"Title=Wmunu total" \
    Sh_2211_Wmunu_maxHTpTV2_BFilter.700341_bb.yoda:"Title=Wmunu bb" \
    Sh_2211_Wmunu_maxHTpTV2_BFilter.700341_nbb.yoda:"Title=Wmunu not bb" \
    --pdf --no-weights --errs -o plots_sh_test

rivet-mkhtml \
    /data0/miham/data/Yoda/vhf_test.v4/user.mmuskinj.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.vhf_test.v4_EXT0.yoda.gz:"Title=Wmunu FxFx total" \
    /data0/miham/data/Yoda/vhf_test.v4/user.mmuskinj.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.vhf_test.v4_EXT0_bb.yoda.gz:"Title=bb component" \
    /data0/miham/data/Yoda/vhf_test.v4/user.mmuskinj.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.vhf_test.v4_EXT0_nbb.yoda.gz:"Title=not bb" \
    --pdf --no-weights --errs -o plots_fxfx_test

rivet-mkhtml \
    /data0/miham/data/Yoda/vhf_test.v5/user.mmuskinj.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.vhf_test.v6_EXT0-scaled.yoda.gz:"Title=Sh2.2.11 Z+jets BFilt" \
    /data0/miham/data/Yoda/vhf_test.v5/user.mmuskinj.506196.MGPy8EG_Zmumu_FxFx_3jets_HT2bias_BFilter.vhf_test.v6_EXT0-scaled.yoda.gz:"Title=MG(FxFx) Z+jets BFilt" \
    --no-rivet-refs --pdf --no-weights --errs -o plots_zjets

rivet-mkhtml \
    /data0/miham/data/Yoda/vhf_test.v5/user.mmuskinj.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.vhf_test.v6_EXT0-scaled.yoda.gz:"Title=Sh2.2.11 W+jets BFilt" \
    /data0/miham/data/Yoda/vhf_test.v5/user.mmuskinj.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.vhf_test.v6_EXT0-scaled.yoda.gz:"Title=MG(FxFx) W+jets BFilt" \
    /data0/miham/data/Yoda/vhf_test.v6/user.mmuskinj.mc15_13TeV.364xxx.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV_BFilter.vhf_test.v6_EXT0.yoda.gz:"Title=Sh2.2.1 W+jets BFilt" \
    /data0/miham/data/Yoda/vhf_test.v6/user.mmuskinj.700095.Sh_2210_Wmunubb_EnhLogpTV.vhf_test.v6_EXT0-scaled.yoda.gz:"Title=Sh2.2.10 W+bb 4FS" \
    --no-rivet-refs --pdf --no-weights --errs -o plots_wjets

rivet-mkhtml \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz:"Title=Sh2.2.11 W+jets BFilt" \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz:"Title=MG(FxFx) W+jets BFilt" \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.3636XX.MGPy8EG_N30NLO_Wmunu_BFilter.vhf_test.v3.yoda.gz:"Title=MG(CKKW) W+jets BFilt" \
    --no-rivet-refs --pdf --no-weights --errs -o plots_wjets2

rivet-mkhtml \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz:"Title=Sh2.2.11 W+jets BFilt" \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz:"Title=MG(FxFx) W+jets BFilt" \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.700386.Sh_2211_Wmunu_maxHTpTV2_CKKW15.vhf_test.v3_EXT0-scaled.yoda.gz:"Title=Sh2.2.11 W+jets CKKW15" \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.700387.Sh_2211_Wmunu_maxHTpTV2_CKKW30.vhf_test.v3_EXT0-scaled.yoda.gz:"Title=Sh2.2.11 W+jets CKKW30" \
    --no-rivet-refs --pdf --no-weights --errs -o plots_wjets3

rivet-mkhtml \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz:"Title=Sh2.2.11 W+jets BFilt" \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.508982.MGPy8EG_Wmunu_FxFx_3jets_HT2bias_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz:"Title=MG(FxFx) W+jets BFilt" \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.363626.MGPy8EG_N30NLO_Wmunu_Ht0_70_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz:"Title=Ht0_70_BFilter" \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.363629.MGPy8EG_N30NLO_Wmunu_Ht70_140_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz:"Title=Ht70_140_BFilter" \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.363632.MGPy8EG_N30NLO_Wmunu_Ht140_280_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz:"Title=Ht140_280_BFilter" \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.363635.MGPy8EG_N30NLO_Wmunu_Ht280_500_BFilter.vhf_test.v3_EXT0-scaled.yoda.gz:"Title=Ht280_500_BFilter" \
    --no-rivet-refs --pdf --no-weights --errs -o plots_wjets4

rivet-mkhtml \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.70034X.Sh_2211_Wmunu_maxHTpTV2.vhf_test.v3_EXT0.yoda.gz:"Title=Sh2.2.11 W+jets" \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.50898X.MGPy8EG_Wmunu_FxFx_3jets_HT2bias.vhf_test.v3_EXT0.yoda.gz:"Title=MGPy8 (FxFx)" \
    /data0/miham/data/Yoda/vhf_test.v3/user.mmuskinj.3636XX.MGPy8EG_N30NLO_Wmunu.vhf_test.v3.yoda.gz:"Title=MGPy8 (CKKW)" \
    --no-rivet-refs --pdf --no-weights --errs -o plots_wjets_test

rivet-mkhtml \
    /data0/miham/data/Yoda/vhf_test.v6/user.mmuskinj.100000.rikkert.vhf_test.v6.p2_EXT0.yoda.gz:"Title=MG FxFx" \
    --no-rivet-refs --pdf --no-weights --errs -o plots_wjets_rikkert
