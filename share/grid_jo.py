import os
from Rivet_i.Rivet_iConf import Rivet_i
from AthenaCommon.AlgSequence import AlgSequence
import AthenaPoolCnvSvc.ReadAthenaPool
theApp.EvtMax = -1


job = AlgSequence()

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD']

rivet.Analyses += ['VHF_TEST']
rivet.RunName = ''
rivet.HistoFile = 'MyOutput.yoda.gz'
rivet.CrossSection = 1.0
rivet.IgnoreBeamCheck = True
rivet.SkipWeights = True
job += rivet
