import os
from Rivet_i.Rivet_iConf import Rivet_i
from AthenaCommon.AlgSequence import AlgSequence
import AthenaPoolCnvSvc.ReadAthenaPool
theApp.EvtMax = -1

svcMgr.EventSelector.InputCollections = [
    # '/data0/miham/data/EVNT/mc15_13TeV.700319.Sh_2211_Wtaunu_H_maxHTpTV2.evgen.EVNT.e8338/EVNT.28648415._005032.pool.root.1',
    '/data0/miham/data/EVNT/mc16_13TeV.700311.Sh_2211_Zmumu_maxHTpTV2.merge.EVNT.e8338_e7400/EVNT.26088735._003316.pool.root.1',
]

job = AlgSequence()

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD']

# 8 TeV W+jets
# https://gitlab.cern.ch/atlas-physics/pmg/rivet-routines/-/blob/master/STDM-2016-14_Wjets_8TeV/ATLAS_2018_I1635273.cc
rivet.Analyses += ['WJets_test']
rivet.RunName = ''
# rivet.HistoFile = 'Sh_2211_Wtaunu_H_maxHTpTV2_70MET150.700543.yoda.gz'
# rivet.HistoFile = 'Sh_2211_Wtaunu_H_maxHTpTV2_METorTAU150.700544.yoda.gz'
# rivet.HistoFile = 'Sh_2211_Wtaunu_H_maxHTpTV2.700319.yoda.gz'
rivet.HistoFile = 'Sh_2211_Zmumu_maxHTpTV2.700311.yoda.gz'
rivet.CrossSection = 1.0
rivet.IgnoreBeamCheck = False
rivet.SkipWeights=True
job += rivet
